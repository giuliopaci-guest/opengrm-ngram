#!/bin/sh
PATH=../../src/bin:../../src/test:"$PATH"
for i in `ls *.1 | sed -e 's/.1$//g'`;
do
    help2man $i \
             --no-discard-stderr \
             --no-info \
             -S "OpenGrm NGram library 1.3.2" \
             --version-string="1.3.2" \
             -n "`$i --help 2>&1 | head -n 1 | sed -e 's/-/\\-/g'`" \
        | grep -v "^Flags from:\|^PROGRAM FLAGS\|^LIBRARY FLAGS" \
        | sed -e 's/^\\fB.*/.HP\n&\n.IP/g' -e 's/^Usage: \(.*\)/.SH SYNOPSIS\n\1\n.SH OPTIONS/g' \
        | sed -e 's#.*.libs/lt\\-##g' -e 's#.*.libs/##g' \
        | uniq > $i.1 ;
done

