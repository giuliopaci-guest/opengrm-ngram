Source: opengrm-ngram
Section: science
Priority: optional
Maintainer: Giulio Paci <giuliopaci@gmail.com>
Build-Depends: cdbs,
 autotools-dev,
 licensecheck,
 libtool,
 automake,
 autoconf,
 dh-buildinfo,
 debhelper (>= 9),
 dpkg-dev (>= 1.16.1~),
 libfst-dev (>= 1.6.3),
 libfst-tools (>= 1.6.3)
Standards-Version: 4.0.1
Vcs-Git: https://anonscm.debian.org/git/collab-maint/opengrm-ngram.git
Vcs-Browser: https://anonscm.debian.org/git/collab-maint/opengrm-ngram.git
Homepage: http://www.openfst.org/twiki/bin/view/GRM/NGramLibrary

Package: libngram-tools
Architecture: any
Depends: libngram2 (= ${binary:Version}),
	 ${misc:Depends},
	 ${shlibs:Depends}
Recommends: libfst-tools
Description: OpenGRM n-gram Language Modeling toolkit
 The OpenGrm NGram library is used for making and modifying n-gram
 language models encoded as weighted finite-state transducers
 (FSTs). The library includes operations for counting, smoothing,
 pruning, applying, and evaluating n-gram language models.
 .
 This package provides the command line tools.

Package: libngram-dev
Section: libdevel
Architecture: any
Depends: libngram2 (= ${binary:Version}),
         ${misc:Depends}
Description: OpenGRM n-gram Language Modeling toolkit - development files
 The OpenGrm NGram library is used for making and modifying n-gram
 language models encoded as weighted finite-state transducers
 (FSTs). The library includes operations for counting, smoothing,
 pruning, applying, and evaluating n-gram language models.
 .
 This package provides development headers for OpenGRM n-gram.

Package: libngram2
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: OpenGRM n-gram Language Modeling toolkit - runtime library
 The OpenGrm NGram library is used for making and modifying n-gram
 language models encoded as weighted finite-state transducers
 (FSTs). The library includes operations for counting, smoothing,
 pruning, applying, and evaluating n-gram language models.
 .
 This package contains the OpenGRM n-gram shared library.
